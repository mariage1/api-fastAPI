import pytest
from sqlalchemy.orm import Session

from app import logger
from app.database import Base, engine, SessionLocal
from test.mock.famille_mock import init_famille, delete_famille
from test.mock.invite_mock import init_invites, delete_invites
from test.mock.marie_mock import init_marier, delete_marier


@pytest.fixture(scope="session", autouse=True)
def create_db():
    logger.debug("Fixture")
    Base.metadata.create_all(engine)
    db: Session = SessionLocal()
    init_marier(db)
    init_famille(db)
    init_invites(db)
    yield
    delete_invites(db)
    delete_marier(db)
    delete_famille(db)
    Base.metadata.drop_all(engine)
    logger.debug("Exit Fixture")

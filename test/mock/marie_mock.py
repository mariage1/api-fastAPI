from sqlalchemy.orm import Session

from app.models.users_models import Marie

mock_marie_one = Marie(
    last_name="last_name_marie_one",
    first_name="first_name_marie_one",
    hashed_password="fake_hashed_password",
)

mock_marie_two = Marie(
    last_name="last_name_marie_two",
    first_name="first_name_marie_two",
    hashed_password="fake_hashed_password",
)

all_marier = [mock_marie_one, mock_marie_two]


def init_marier(db: Session):
    db.add_all(all_marier)
    db.commit()
    for marier in all_marier:
        db.refresh(marier)


def delete_marier(db: Session):
    for marier in all_marier:
        db.delete(marier)
    db.commit()

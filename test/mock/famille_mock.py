from sqlalchemy.orm import Session

from app.models.users_models import Famille

mock_famille_one = Famille(
    name="famille_one"
)

mock_famille_two = Famille(
    name="famille_two"
)

all_famille = [mock_famille_one, mock_famille_two]


def init_famille(db: Session):
    db.add_all(all_famille)
    db.commit()
    for famille in all_famille:
        db.refresh(famille)


def delete_famille(db: Session):
    for famille in all_famille:
        db.delete(famille)
    db.commit()

from sqlalchemy.orm import Session

from app.models.enum.categorie import Categorie
from app.models.enum.importance import Importance
from app.models.users_models import Invite
from test.mock.famille_mock import mock_famille_one, mock_famille_two
from test.mock.marie_mock import mock_marie_one, mock_marie_two

mock_invite_one = Invite(
    last_name="last_name_invite_one",
    first_name="first_name_invite_one",
    hashed_password="fake_hashed_password",
    email="invite_one@test.com",
    importance=Importance('I'),
    categorie=Categorie('Autre')
)

mock_invite_one.famille = mock_famille_one
mock_invite_one.maries_groupe = mock_marie_one

mock_invite_two = Invite(
    last_name="last_name_invite_one",
    first_name="first_name_invite_one",
    hashed_password="fake_hashed_password",
    email="invite_two@test.com",
    importance=Importance('I'),
    categorie=Categorie('Autre')
)

mock_invite_two.famille = mock_famille_one
mock_invite_two.maries_groupe = mock_marie_one

mock_invite_three = Invite(
    last_name="last_name_invite_one",
    first_name="first_name_invite_one",
    hashed_password="fake_hashed_password",
    email="invite_three@test.com",
    importance=Importance('I'),
    categorie=Categorie('Autre')
)

mock_invite_three.famille = mock_famille_two
mock_invite_three.maries_groupe = mock_marie_two

all_invites = [
    mock_invite_one,
    mock_invite_two,
    mock_invite_three,
]


def init_invites(db: Session):
    db.add_all(all_invites)
    db.commit()
    for invite in all_invites:
        db.refresh(invite)


def delete_invites(db: Session):
    for invite in all_invites:
        db.delete(invite)
    db.commit()

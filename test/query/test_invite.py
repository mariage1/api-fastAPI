from app.services.oauth2_service import create_access_token
from test import client_app
from test.mock.invite_mock import all_invites
from test.mock.marie_mock import mock_marie_one


def test_invites():
    token = create_access_token(
        data={
            "sub": str(mock_marie_one.uuid),
            "role": mock_marie_one.role,
        })

    payload = "{\"query\":\"{\\r\\n    invites{\\r\\n        uuid\\r\\n         firstName\\r\\n         " \
              "lastName\\r\\n    }\\r\\n}\",\"variables\":{}} "
    headers = {
        'Authorization': 'Bearer '+token,
        'Content-Type': 'application/json'
    }
    executed = client_app.post(
        url="/graphql/",
        data=payload,
        headers=headers

    )

    list_invites_result = []

    for invite in all_invites:
        list_invites_result.append(
            {
                'uuid': str(invite.uuid),
                "firstName": invite.first_name,
                "lastName": invite.last_name,
            }
        )

    assert executed.json() == {
        "data": {
            "invites": list_invites_result
        }
    }

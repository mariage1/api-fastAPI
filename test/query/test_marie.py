from app.services.oauth2_service import create_access_token
from test import client_app
from test.mock.marie_mock import mock_marie_one, mock_marie_two


def test_query_marie():

    token = create_access_token(
            data={
                "sub": str(mock_marie_one.uuid),
                "role": mock_marie_one.role,
            })

    headers = {
        'Authorization': 'Bearer '+token,
        'Content-Type': 'application/json'
    }
    payload = "{\"query\":\"query test($uuid: String) {" \
              "\\r\\n  marie(marierUuid: $uuid ){\\r\\n" \
              "    uuid\\r\\n    firstName\\r\\n    lastName\\r\\n  }}\"," \
              "\"variables\":{\"uuid\":\""+str(mock_marie_one.uuid)+"\"}}"

    executed = client_app.post(
        url="/graphql/",
        data=payload,
        headers=headers
    )

    assert executed.json() == {
        "data": {
            "marie": {
                "uuid": str(mock_marie_one.uuid),
                "firstName": mock_marie_one.first_name,
                "lastName": mock_marie_one.last_name
            }
        }
    }

    
def test_query_maries():
    token = create_access_token(
        data={
            "sub": str(mock_marie_one.uuid),
            "role": mock_marie_one.role,
        })

    headers = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }

    payload="{\"query\":\"{\\r\\n      maries{\\r\\n        uuid\\r\\n        firstName\\r\\n        lastName\\r\\n      }}\",\"variables\":{}}"

    executed = client_app.post(
        url="/graphql/",
        data=payload,
        headers=headers
    )

    assert executed.json()  == {
        "data": {
            "maries": [{
                "uuid": str(mock_marie_one.uuid),
                "firstName": mock_marie_one.first_name,
                "lastName": mock_marie_one.last_name
            },
                {
                    "uuid": str(mock_marie_two.uuid),
                    "firstName": mock_marie_two.first_name,
                    "lastName": mock_marie_two.last_name
                },
            ]
        }
    }

import starlette
from starlette.websockets import WebSocket

from app import logger
from app.services.ws_service.ws_state_service import Context, StateStart


async def websocket_endpoint(websocket: WebSocket):
    context = Context(StateStart(
        ws=websocket
    ))
    last: bool = False
    try:
        while not last:
            last = await context.action()
    except starlette.websockets.WebSocketDisconnect:
        logger.error("Web socket Disconnect")
        await websocket.close()
        raise Exception("Web socket Disconnect")

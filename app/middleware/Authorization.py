from functools import wraps

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt, JWTError

from app import logger, setting
from app.services.util_service import convert

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="rest/security/token")

credentials_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)

privilege_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate role",
    headers={"WWW-Authenticate": "Bearer"},
)


def graphql_security_token(func):
    @wraps(func)
    async def get_token_security(self, info, *args, **kwargs):
        header_di = {}
        try:
            logger.debug("graphql token security")
            header = info.context['request']['headers']
            convert(header, header_di)
            token_bearer = header_di[b'authorization'].pop().decode('utf-8')
            token = token_bearer.split("Bearer ").pop()
        except Exception:
            raise credentials_exception

        return func(self, info, token, *args, **kwargs)

    return get_token_security


def graphql_security_check_privilege(privilege_roles):
    def callable(func):
        @wraps(func)
        @graphql_security_token
        async def chek_security_privilege(self, info, token, *args, **kwargs):
            logger.debug("chek privilege")
            try:
                payload = jwt.decode(
                    token, setting.secret_key, algorithms=[
                        setting.token_jwt_algo])
                privilage = False
                roles = payload.get("role")
                for role in roles:
                    if role in privilege_roles:
                        privilage = True
                        break
                if not privilage:
                    raise privilege_exception
            except JWTError:
                raise credentials_exception
            return func(self, info, token, *args, **kwargs)

        return chek_security_privilege

    return callable


async def rest_security_token_validator(token: str = Depends(oauth2_scheme)):
    logger.debug("Rest token security")
    try:
        payload = jwt.decode(
            token, setting.secret_key, algorithms=[
                setting.token_jwt_algo])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception

    return payload


async def rest_security_all_privilege(payload: dict = Depends(rest_security_token_validator)):
    logger.debug("Rest role security")
    privilege_role = ['maries', 'admin']
    if payload.get("role") not in privilege_role:
        raise privilege_exception
    return True

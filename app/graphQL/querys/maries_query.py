import uuid

import graphene
from sqlalchemy.orm import Session

from app.database import SessionLocal
from app.graphQL.schemas.marie_schema.marie_schema import MarieSchema
from app.crud.maries_crud import get_marier, get_maries
from app.middleware.Authorization import graphql_security_token
from app.models.users_models import Marie
from app.services.model_to_schema_service import traduction_marier


class MariesQuery(graphene.ObjectType):
    marie = graphene.Field(MarieSchema,
                           marier_uuid=graphene.String()
                           )
    maries = graphene.List(MarieSchema)

    @staticmethod
    @graphql_security_token
    def resolve_marie(self, info, token,
                      marier_uuid: str):
        marie: Marie or None = None
        result = None
        db: Session = SessionLocal()

        if marier_uuid is not None:
            marier_uuid = uuid.UUID(marier_uuid)
            marie = get_marier(db=db, marier_uuid=marier_uuid)

        if marie is not None:
            result = traduction_marier(marie)
        db.close()
        return result

    @staticmethod
    @graphql_security_token
    def resolve_maries(self, info, token):
        db: Session = SessionLocal()
        maries = get_maries(db=db)
        result = None

        if maries is not None:
            result = []
            for marie in maries:
                result.append(traduction_marier(marie))
        db.close()
        return result

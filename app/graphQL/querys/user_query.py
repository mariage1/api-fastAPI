import graphene
from sqlalchemy.orm import Session

from app.database import SessionLocal
from app.graphQL.schemas.invite_schemas.invite_schema import InviteSchema
from app.graphQL.schemas.marie_schema.marie_schema import MarieSchema
from app.graphQL.schemas.user_schemas.user_union_schema import UserUnion
from app.middleware.Authorization import graphql_security_token
from app.models.users_models import Invite, Marie
from app.services.oauth2_service import get_current_user


class CurantUserQuery(graphene.ObjectType):
    curant_user = graphene.Field(UserUnion)

    @staticmethod
    @graphql_security_token
    def resolve_curant_user(self, info, token):
        db: Session = SessionLocal()
        result = None
        user = get_current_user(db, token)
        if isinstance(user, Invite):
            result = InviteSchema()
            result.from_user(db, user)
        elif isinstance(user, Marie):
            result = MarieSchema()
            result.from_user(user)
        else:
            raise Exception(user)

        return result

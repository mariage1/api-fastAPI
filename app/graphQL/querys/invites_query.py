import uuid

import graphene
from sqlalchemy.orm import Session

from app.crud.invite_crud import get_invites, get_invite, get_invite_by_email
from app.database import SessionLocal
from app.graphQL.schemas.invite_schemas.invite_schema import InviteSchema
from app.middleware.Authorization import graphql_security_check_privilege, graphql_security_token
from app.models.users_models import Invite
from app.services.model_to_schema_service import traduction_invite


class InvitesQuery(graphene.ObjectType):
    invites = graphene.List(InviteSchema)
    invite = graphene.Field(InviteSchema,
                            invite_uuid=graphene.String(),
                            invite_email=graphene.String())

    @staticmethod
    @graphql_security_token
    def resolve_invite(self, info, token,
                       invite_uuid: str = None,
                       invite_email: str = None):

        invite: Invite or None = None
        result = None
        db: Session = SessionLocal()

        if invite_uuid is not None:
            invite_uuid = uuid.UUID(invite_uuid)
            invite = get_invite(
                db=db,
                invite_uuid=invite_uuid)

        if invite_email is not None \
                and invite is None:
            invite = get_invite_by_email(
                db=db,
                email=invite_email)

        if invite is not None:
            result = traduction_invite(invite)

        db.close()
        return result

    @staticmethod
    @graphql_security_check_privilege(privilege_roles=['MARIES'])
    def resolve_invites(self, info, token):
        results = None
        db: Session = SessionLocal()
        invites = get_invites(db=db)

        if invites is not None:
            results = []
            for invite in invites:
                results.append(traduction_invite(invite))
        db.close()
        return results

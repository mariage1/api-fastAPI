import graphene


class FamilleInputSchema(graphene.InputObjectType):
    uuid = graphene.String()
    email = graphene.String()
    name = graphene.String()

import graphene

from app.crud.famille_crud import get_famille
from app.database import SessionLocal
from app.services.model_to_schema_service import traduction_invite


def resolver_membres(famille, info):
    if isinstance(famille, dict):
        famille = get_famille(SessionLocal(), famille["uuid"])
    return [traduction_invite(invite) for invite in famille.membres]


class FamilleSchema(graphene.ObjectType):
    uuid = graphene.UUID()
    name = graphene.String()

    membres = graphene.List(lambda: InviteSchema, resolver=resolver_membres)


from app.graphQL.schemas.invite_schemas.invite_schema import InviteSchema

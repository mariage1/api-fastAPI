import graphene

from app.graphQL.schemas.invite_schemas.invite_schema import InviteSchema
from app.graphQL.schemas.marie_schema.marie_schema import MarieSchema


class UserUnion(graphene.Union):
    class Meta:
        types = (InviteSchema, MarieSchema)

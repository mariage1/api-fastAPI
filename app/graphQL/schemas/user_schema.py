import graphene


class UserSchema(graphene.Interface):
    uuid = graphene.UUID()
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()

    datetime_create_user = graphene.DateTime()
    datetime_last_connection = graphene.DateTime()

import graphene


class MarieInputSchema(graphene.InputObjectType):
    uuid = graphene.String(required=True)
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()

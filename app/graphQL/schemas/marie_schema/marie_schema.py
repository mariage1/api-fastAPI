from app.graphQL.schemas.invite_schemas.invite_schema import InviteSchema
import graphene

from app.crud.maries_crud import get_marier
from app.database import SessionLocal
from app.graphQL.schemas.user_schema import UserSchema
from app.models.users_models import Marie
from app.services.model_to_schema_service import traduction_invite


def resolver_invites(marie, info):
    if isinstance(marie, dict):
        marie = get_marier(marie["uuid"], SessionLocal())
    return [traduction_invite(invite) for invite in marie.invites]


class MarieSchema(graphene.ObjectType):
    invites = graphene.List(lambda: InviteSchema, resolver=resolver_invites)

    class Meta:
        interfaces = (UserSchema,)
        model = Marie

    def from_user(self, user: Marie):
        self.uuid = user.uuid
        self.last_name = user.last_name
        self.first_name = user.first_name
        self.email = user.email

        self.datetime_create_user = user.datetime_create_user
        self.datetime_last_connection = user.datetime_last_connection

        self.invites = user.invites

import graphene


class LoginInputSchema(graphene.InputObjectType):
    email = graphene.String(required=True)
    password = graphene.String(required=True)

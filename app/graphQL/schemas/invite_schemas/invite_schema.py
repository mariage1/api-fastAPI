import graphene
from sqlalchemy.orm import Session

from app.graphQL.schemas.user_schema import UserSchema
from app.services.model_to_schema_service import traduction_marier, traduction_famille
from app.models.users_models import Invite

from app.crud.famille_crud import get_famille
from app.crud.maries_crud import get_marier


def resolve_marie(invite, info):
    if isinstance(invite, dict):
        return traduction_marier(invite['marie'])
    return traduction_marier(invite.marie)


def resolve_famille(invite, info):
    if isinstance(invite, dict):
        return traduction_famille(invite['famille'])
    return traduction_famille(invite.famille)


class InviteSchema(graphene.ObjectType):
    has_validate = graphene.Boolean()
    categorie = graphene.String()
    importance = graphene.String()

    marie = graphene.Field(lambda: MarieSchema, resolver=resolve_marie)
    famille = graphene.Field(lambda: FamilleSchema, resolver=resolve_famille)

    class Meta:
        interfaces = (UserSchema,)
        model = Invite

    def from_user(self, db: Session, user: Invite):
        self.uuid = user.uuid
        self.last_name = user.last_name
        self.first_name = user.first_name
        self.email = user.email

        self.datetime_create_user = user.datetime_create_user
        self.datetime_last_connection = user.datetime_last_connection

        self.has_validate = user.has_validate
        self.categorie = user.categorie.value
        self.importance = user.importance.value

        self.marie = get_marier(db=db, marier_uuid=user.marie_uuid)
        self.famille = get_famille(db=db, uuid=user.famille_uuid)


from app.graphQL.schemas.famille_schemas.famille_schema import FamilleSchema
from app.graphQL.schemas.marie_schema.marie_schema import MarieSchema

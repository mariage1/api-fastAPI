import graphene
from app.graphQL.schemas.famille_schemas.famille_input_schema import FamilleInputSchema


class InviteInputSchema(graphene.InputObjectType):
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()
    password = graphene.String()
    categorie = graphene.String()
    importance = graphene.String()
    famille = graphene.Field(FamilleInputSchema)
    marie_first_name = graphene.String()


class InviteUpdateSchema(InviteInputSchema):
    uuid = graphene.String()
    has_validate = graphene.Boolean()

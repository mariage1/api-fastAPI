from datetime import timedelta

import graphene

from app import setting
from app.graphQL.schemas.login_schemas.login_input_schema import LoginInputSchema
from app.services.oauth2_service import authenticate_user, create_access_token


class MutationLogin(graphene.Mutation):
    class Arguments:
        login_info = LoginInputSchema(required=True)

    stats = graphene.Boolean()
    message = graphene.String()
    token = graphene.String()
    token_type = graphene.String()

    def mutate(self, info,
               login_info):
        user = authenticate_user(
            invite_email=login_info.email,
            password=login_info.password)

        if not user:
            return MutationLogin(
                stats=False,
                message="Error in login/password",
                token=None,
                token_type=None
            )
        access_token_expires = timedelta(hours=setting.token_jwt_expire_hours)
        access_token = create_access_token(
            data={
                "sub": str(user.uuid),
                "role": [user.role.upper()],
            }, expires_delta=access_token_expires
        )

        return MutationLogin(
            stats=True,
            message="Login is done",
            token=access_token,
            token_type="Bearer"
        )

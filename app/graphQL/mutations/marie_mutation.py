import graphene
from sqlalchemy.orm import Session

from app.crud.maries_crud import update_marie
from app.database import SessionLocal
from app.graphQL.schemas.marie_schema.marie_input_schema import MarieInputSchema
from app.middleware.Authorization import graphql_security_token


class MutationMarie(graphene.Mutation):
    class Arguments:
        marie_data = MarieInputSchema(required=True)

    stats = graphene.Boolean()
    message = graphene.String()

    @graphql_security_token
    def mutate(self, info, token,
               marie_data: MarieInputSchema):
        db: Session = SessionLocal()
        new_marie = {
            'uuid': marie_data.uuid
        }
        if marie_data.last_name:
            new_marie['last_name'] = marie_data.last_name
        if marie_data.first_name:
            new_marie['first_name'] = marie_data.first_name
        if marie_data.email:
            new_marie['email'] = marie_data.email

        stat, message = update_marie(
            db=db,
            new_marie=new_marie
        )

        if not stat:
            raise Exception(message)

        return MutationMarie(stats=stat, message=message)

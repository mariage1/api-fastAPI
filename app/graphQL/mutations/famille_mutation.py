import graphene
from sqlalchemy.orm import Session

from app import logger
from app.crud.famille_crud import update_famille
from app.database import SessionLocal
from app.graphQL.schemas.famille_schemas.famille_input_schema import FamilleInputSchema
from app.middleware.Authorization import graphql_security_token
from app.models.users_models import Famille


class MutationFamille(graphene.Mutation):
    class Arguments:
        famille_data = FamilleInputSchema(required=True)

    stats = graphene.Boolean()
    message = graphene.String()

    @graphql_security_token
    def mutate(self, info, token,
               famille_data: FamilleInputSchema):
        db: Session = SessionLocal()

        stat, message = update_famille(
            db=db,
            new_famille=Famille(
                uuid=famille_data.uuid,
                name=famille_data.name,
                famille_email=famille_data.email
            )
        )

        if not stat:
            logger.error(message)

        return MutationFamille(stats=stat, message=message)

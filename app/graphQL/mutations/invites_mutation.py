from copy import copy

import graphene
from sqlalchemy.orm import Session

from app import logger
from app.middleware.Authorization import graphql_security_token, graphql_security_check_privilege
from app.services.oauth2_service import get_password_hash
from app.crud.invite_crud import create_invite, create_invites, delete_invites, update_invite
from app.database import SessionLocal
from app.graphQL.schemas.invite_schemas.invite_input_schema import InviteInputSchema, InviteUpdateSchema


class CreateInvite(graphene.Mutation):
    class Arguments:
        invite_data = InviteInputSchema()

    uuid = graphene.UUID()

    def mutate(self, info,
               invite_data):
        db: Session = SessionLocal()
        invite, message = create_invite(db=db,
                                        first_name=invite_data.first_name,
                                        last_name=invite_data.last_name,
                                        email=invite_data.email,
                                        hased_password=get_password_hash(invite_data.password),
                                        categorie=invite_data.categorie,
                                        famille_name=invite_data.famille.name if invite_data.famille.name else None,
                                        marie_first_name=invite_data.marie_first_name,
                                        importance=invite_data.importance)

        db.close()
        if invite is False:
            raise logger.error(message)

        return CreateInvite(uuid=invite.uuid)


class CreateInvites(graphene.Mutation):
    class Arguments:
        invites_data = graphene.List(InviteInputSchema)

    stats = graphene.Boolean()
    message = graphene.String()

    @graphql_security_check_privilege(privilege_roles=['MARIES'])
    def mutate(self, info,
               invites_data):

        invites = []
        db: Session = SessionLocal()

        for invite_data in invites_data:
            invite = {}
            info = {
                "first_name": invite_data.first_name,
                "last_name": invite_data.last_name,
                "email": invite_data.email,
                "hashed_password": get_password_hash(invite_data.password),
                "categorie": invite_data.categorie
            }

            if invite_data.marie_first_name is not None:
                info["marie"] = invite_data.marie_first_name

            if invite_data.famille is not None:
                invite["famille"] = invite_data.famille

            invite["info"] = copy(info)
            invites.append(copy(invite))

        stat, message = create_invites(db=db, invites=invites)
        if stat is False:
            raise logger.error(message)
        db.close()
        return CreateInvites(stats=stat, message=message)


class MutationInvite(graphene.Mutation):
    class Arguments:
        invite_data = InviteUpdateSchema(required=True)
        famille_action = graphene.String()

    stats = graphene.Boolean()
    message = graphene.String()

    @graphql_security_token
    def mutate(self, info, token,
               invite_data: InviteUpdateSchema, famille_action: str = None):
        db: Session = SessionLocal()

        if invite_data.uuid is not None:

            stats, message = update_invite(db=db,
                                           uuid=invite_data.uuid,
                                           first_name=invite_data.first_name,
                                           last_name=invite_data.last_name,
                                           categorie=invite_data.categorie,
                                           email=invite_data.email,
                                           famille_name=invite_data.famille.name if invite_data.famille else None,
                                           has_validate=invite_data.has_validate,
                                           importance=invite_data.importance,
                                           famille_action=famille_action)

        else:
            invite, message = create_invite(db=db,
                                            first_name=invite_data.first_name,
                                            last_name=invite_data.last_name,
                                            categorie=invite_data.categorie,
                                            email=invite_data.email,
                                            famille_name=invite_data.famille.name,
                                            importance=invite_data.importance,
                                            has_validate=invite_data.has_validate,
                                            hased_password=get_password_hash(
                                                invite_data.first_name + '#' + invite_data.last_name))

            stats = False if (invite is None) else True

        if not stats:
            logger.error(message)

        db.close()

        return CreateInvites(stats=stats, message=message)


class DeleteInvite(graphene.Mutation):
    class Arguments:
        invites_uuids = graphene.List(graphene.UUID, required=True)

    stats = graphene.Boolean()
    message = graphene.String()

    @graphql_security_check_privilege(privilege_roles=['MARIES'])
    def mutate(self, info, token, invites_uuids):
        db: Session = SessionLocal()
        delete_invites(db=db, uuids=invites_uuids)
        db.close()
        return DeleteInvite(stats=True, message='Invites Delete')

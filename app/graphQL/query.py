from app.graphQL.querys.invites_query import InvitesQuery
from app.graphQL.querys.maries_query import MariesQuery
from app.graphQL.querys.user_query import CurantUserQuery


class Query(InvitesQuery, MariesQuery, CurantUserQuery):
    pass

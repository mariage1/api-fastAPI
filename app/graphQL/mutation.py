import graphene

from app.graphQL.mutations.famille_mutation import MutationFamille
from app.graphQL.mutations.invites_mutation import CreateInvite, CreateInvites, MutationInvite, DeleteInvite
from app.graphQL.mutations.login_mutation import MutationLogin
from app.graphQL.mutations.marie_mutation import MutationMarie


class Mutation(graphene.ObjectType):
    create_invite = CreateInvite.Field()
    create_invites = CreateInvites.Field()
    mutation_invite = MutationInvite.Field()
    delete_invite = DeleteInvite.Field()
    mutation_login = MutationLogin.Field()
    mutation_famille = MutationFamille.Field()
    mutation_marie = MutationMarie.Field()

from copy import copy

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from app import logger
from app.crud.famille_crud import get_famille_by_name, create_famille
from app.crud.maries_crud import get_marier_by_first_name
from app.models.enum.categorie import Categorie
from app.models.enum.importance import Importance
from app.models.users_models import Invite, Famille


def create_invite(
        db: Session,
        first_name: str,
        last_name: str,
        email: str,
        hased_password: str,
        categorie: str,
        importance: str,
        famille_name: str = None,
        marie_first_name: str = None,
        has_validate: bool = False):
    db_invite = Invite(
        first_name=first_name,
        last_name=last_name,
        email=email,
        hashed_password=hased_password,
        categorie=Categorie(categorie),
        importance=Importance(importance),
        has_validate=has_validate
    )

    if famille_name is not None:
        db_famille = get_famille_by_name(db=db, name=famille_name)
        if db_famille is None:
            db_famille = create_famille(db=db, name=famille_name)

        if db_famille is None:
            return False, "Erreur dans la fammile"

        db_invite.famille = db_famille

    if marie_first_name is not None:
        db_marier = get_marier_by_first_name(
            db=db, marier_first_name=marie_first_name)
        if db_marier is None:
            logger.error(
                "Erreur dans le choix du marier, Marier {} inexistant".format(marie_first_name))
            return False, "Erreur dans le choix du marier, Marier {} inexistant".format(
                marie_first_name)
        db_invite.maries_groupe = db_marier

    try:
        db.add(db_invite)
        db.commit()
        db.refresh(db_invite)
    except IntegrityError as err:
        orig = str(err.orig).split("\n")
        logger.error(
            "Erreur dans la creation d'un invite : {} ".format(
                orig[0]))
        logger.debug("{} ".format(orig[0:]))
        return False, "Erreur dans la creation d'un invite"
    logger.debug("inviter créé")
    return db_invite, "invite créé"


def create_invites(db: Session, invites):
    db_invite: Invite or None = None
    list_db_invites = []
    familles_memory = {}
    marie_memory = {}

    for invite in invites:
        famille_name = None
        db_famille = None

        if "famille" in invite:
            famille_name = invite["famille"]

        try:
            infos_invite = invite["info"]
            db_invite = Invite(
                first_name=infos_invite["first_name"],
                last_name=infos_invite["last_name"],
                email=infos_invite["email"],
                hashed_password=infos_invite["hashed_password"],
                categorie=Categorie(infos_invite["categorie"]),
                importance=Importance(infos_invite["importance"])
            )

            if famille_name is not None:
                if famille_name not in familles_memory:
                    db_famille = get_famille_by_name(db=db, name=famille_name)
                    if db_famille is None:
                        db_famille = Famille(name=famille_name)
                        db.add(db_famille)
                    familles_memory[famille_name] = copy(db_famille)

                db_invite.famille = familles_memory[famille_name]
            else:
                return False, "Erreur dans les information de l'invité {}, Un invité doir être lier à une famille" \
                    .format(db_invite.email)

            if "marie" in infos_invite:
                if infos_invite["marie"] not in marie_memory:
                    marier = get_marier_by_first_name(
                        db=db, marier_first_name=infos_invite["marie"])
                    if marier is None:
                        return False, "Erreur dans le choix du marier, Marier {} inexistant" \
                            .format(infos_invite["marie"])
                    marie_memory[infos_invite["marie"]] = marier
                else:
                    marier = marie_memory[infos_invite["marie"]]
                db_invite.maries_groupe = marier
            else:
                return False, "Erreur dans les information de l'invité {}, Un invité doir être lier à l'un des marier" \
                    .format(db_invite.email)

            list_db_invites.append(copy(db_invite))

        except Exception:
            return False, "Erreur dans la creation des Invites"
    try:
        db.add_all(list_db_invites)
        db.commit()
    except IntegrityError as err:
        orig = str(err.orig).split("\n")
        logger.error(
            "Erreur dans la creation d'un invite : {} ".format(
                orig[0]))
        logger.debug("{} ".format(orig[0:]))
        return False, "Erreur dans la creation d'un invite"
    return True, "Invites créés"


def get_invite(db: Session, invite_uuid):
    return db.query(Invite).filter(Invite.uuid == invite_uuid).first()


def get_invites(db: Session, skip: int = 0):
    return db.query(Invite).offset(skip).all()


def get_invite_by_email(db: Session, email):
    return db.query(Invite).filter(Invite.email == email).first()


def get_invite_by_marier(db: Session, marier_uuid):
    return db.query(Invite).filter(Invite.marie_uuid == marier_uuid).all()


def get_invite_by_famille(db: Session, famille_uuid: str):
    return db.query(Invite).filter(Invite.famille_uuid == famille_uuid).all()


def get_uuid_list(db: Session):
    return db.query().with_entities(Invite.uuid).all()


def delete_invites(db: Session, uuids):
    invites = db.query(Invite).filter(Invite.uuid.in_(uuids)).all()
    for invite in invites:
        famille = get_invite_by_famille(
            db=db, famille_uuid=invite.famille_uuid)
        db.delete(invite)
        if len(famille) == 1:
            logger.debug(
                "famille {} deleted because it is empty".format(
                    invite.famille.name))
            db.delete(invite.famille)
    db.commit()


def update_invite(db: Session,
                  uuid: str,
                  first_name: str,
                  last_name: str,
                  email: str,
                  categorie: str,
                  importance: str,
                  famille_action: str,
                  famille_name: str,
                  has_validate: bool
                  ):
    db_invite: Invite = get_invite(db=db, invite_uuid=uuid)

    if first_name:
        db_invite.first_name = first_name
    if last_name:
        db_invite.last_name = last_name

    if email:
        db_invite.email = email
    if categorie:
        db_invite.categorie = Categorie(categorie)

    if importance:
        db_invite.importance = Importance(importance)

    if famille_action is not None:
        if famille_action == 'Update':
            db_invite.famille.name = famille_name
        if famille_action == 'New':
            try:
                db_invite.famille = create_famille(db=db, name=famille_name)
            except Exception:
                return False, 'Error in famille creation'
        if famille_action == 'Switch':
            db_invite.famille = get_famille_by_name(db=db, name=famille_name)

    if has_validate is not None:
        db_invite.has_validate = has_validate

    db.add(db_invite)
    db.commit()

    return True, 'Invite is update'

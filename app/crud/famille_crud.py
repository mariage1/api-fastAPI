from uuid import UUID

from sqlalchemy.orm import Session

from app.models.users_models import Famille


def create_famille(db: Session, name: str):
    db_famille = Famille(name=name)
    db.add(db_famille)
    db.commit()
    db.refresh(db_famille)
    return db_famille


def get_famille(db: Session, uuid: UUID):
    return db.query(Famille).filter(Famille.uuid == uuid).first()


def get_famille_by_name(db: Session, name: str):
    return db.query(Famille).filter(Famille.name == name).first()


def update_famille(db: Session, new_famille: Famille):
    famille = get_famille(db, new_famille.uuid)
    if new_famille.name:
        famille.name = new_famille.name
    if new_famille.famille_email:
        famille.famille_email = new_famille.famille_email

    db.add(famille)
    db.commit()
    return True, "Update is done"

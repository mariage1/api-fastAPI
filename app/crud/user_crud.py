from sqlalchemy.orm import Session

from app.crud.invite_crud import get_invite, get_invite_by_email
from app.crud.maries_crud import get_marier, get_marie_by_email


def get_user(db: Session, user_uuid):
    user = get_invite(db, user_uuid)

    if user is not None:
        return user

    user = get_marier(db=db, marier_uuid=user_uuid)

    if user is not None:
        return user

    return None


def get_user_by_email(db: Session, email):
    user = get_invite_by_email(db=db, email=email)

    if user is not None:
        return user

    user = get_marie_by_email(db=db, email=email)

    if user is not None:
        return user

    return None

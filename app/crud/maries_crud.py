from sqlalchemy.orm import Session

from app import logger
from app.models.users_models import Marie


def create_marier(
        first_name: str,
        last_name: str,
        email: str,
        hased_password: str,
        db: Session,
):
    db_marier = Marie(
        first_name=first_name,
        last_name=last_name,
        email=email,
        hashed_password=hased_password
    )
    db.add(db_marier)
    db.commit()
    db.refresh(db_marier)
    return db_marier


def create_maries(liste_maries, db: Session):
    db_maries = []
    for marie in liste_maries:
        db_marier = Marie(
            first_name=marie["first_name"],
            last_name=marie["last_name"],
            email=marie["email"],
            hashed_password=marie["hashed_password"]
        )
        db_maries.append(db_marier)
    db.add_all(db_maries)
    db.commit()


def get_marier(marier_uuid, db: Session):
    return db.query(Marie).filter(Marie.uuid == marier_uuid).first()


def get_marier_by_first_name(marier_first_name, db: Session):
    return db.query(Marie).filter(
        Marie.first_name == marier_first_name).first()


def get_maries(db: Session):
    return db.query(Marie).all()


def get_marie_by_email(email, db: Session):
    return db.query(Marie).filter(Marie.email == email).first()


def update_marie(db: Session, new_marie: dict):
    try:
        marie = get_marier(db=db, marier_uuid=new_marie['uuid'])
    except Exception as e:
        logger.error(str(e))
        return False, "Error in get marier"

    for key, val in new_marie.items():
        setattr(marie, key, val)

    db.add(marie)
    db.commit()
    return True, "Update is done"

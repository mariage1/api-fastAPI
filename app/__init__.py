import logging
from logging.config import dictConfig

from app.config import Setting

setting = Setting()

dictConfig(setting.log_config)

logger = logging.getLogger('foo-logger')

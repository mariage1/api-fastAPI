from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app import setting

if setting.db_url is not None:
    SQLALCHEMY_DATABASE_URL = setting.db_url
else:
    SQLALCHEMY_DATABASE_URL = "postgresql://{}:{}@{}/{}".format(
        setting.db_user,
        setting.db_password,
        setting.db_server_name,
        setting.db_name)

engine = create_engine(SQLALCHEMY_DATABASE_URL, pool_size=10, max_overflow=20)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

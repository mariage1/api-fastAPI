import os

from pydantic import BaseSettings


class Setting(BaseSettings):
    env_name: str = os.getenv("ENV_NAME", "dev")

    db_url: str = os.getenv("DATABASE_URL")
    db_user: str = os.getenv("DB_USER", "postgres")
    db_password: str = os.getenv("DB_PASSWORD", "postgres")
    db_server_name: str = os.getenv("DB_SERVER_NAME", "localhost")
    db_name: str = os.getenv("DB_NAME", "postgres")

    secret_key: str = os.getenv("SECRET_KEY", "your-secret-key")
    token_jwt_algo: str = os.getenv("TOKEN_JWT_ALGORITHM", "HS256")
    token_jwt_expire_hours: int = os.getenv("TOKEN_JWT_EXPIRE", 5)

    client_url: str = os.getenv("CLIENT_URL", "http://localhost/")

    default_marier_password: str = os.getenv(
        "DEFAULT_MARIER_PASSWORD", "your-secret-password")

    gmail_user: str = os.getenv("GMAIL_USER")
    gmail_password: str = os.getenv("GMAIL_PASSWORD")

    debug_level: str = os.getenv("DEBUG_LEVEL", "INFO")

    log_config = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {
                "()": "uvicorn.logging.DefaultFormatter",
                "fmt": "%(levelprefix)s %(message)s",
            },
        },
        "handlers": {
            "default": {
                "formatter": "default",
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stderr",
            },
        },
        "loggers": {
            "foo-logger": {"handlers": ["default"], "level": debug_level},
        },
    }

import datetime
import uuid

from sqlalchemy import Boolean, Column, String, DateTime, ForeignKey, Enum
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from app.database import Base
from app.models.enum.categorie import Categorie
from app.models.enum.importance import Importance


class User(Base):
    __abstract__ = True

    uuid = Column(
        UUID(
            as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
        index=True)
    last_name = Column(String)
    first_name = Column(String)
    email = Column(String)

    datetime_create_user = Column(
        DateTime, default=datetime.datetime.now().isoformat())
    datetime_last_connection = Column(DateTime)

    is_active = Column(Boolean, default=False)
    hashed_password = Column(String)

    role: str


class Marie(User):
    __tablename__ = "maries"

    uuid = Column(
        UUID(
            as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
        index=True)

    invites = relationship(
        "Invite",
        backref="maries_groupe",
        lazy='dynamic',
        cascade="all, delete-orphan")

    role = 'maries'

    __mapper_args__ = {
        'polymorphic_identity': 'maries',
    }


class Famille(Base):
    __tablename__ = "famille"

    uuid = Column(
        UUID(
            as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
        index=True)

    famille_email = Column(String, nullable=True, default="default@mail.com")

    name = Column(String, unique=True, nullable=False)

    membres = relationship(
        "Invite",
        backref="famille",
        lazy='dynamic',
        cascade="all, delete-orphan")


class Invite(User):

    __tablename__ = "invites"

    uuid = Column(
        UUID(
            as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
        index=True)

    categorie = Column(Enum(Categorie))
    has_validate = Column(Boolean, default=False)
    importance = Column(Enum(Importance))

    marie_uuid = Column(
        UUID(
            as_uuid=True),
        ForeignKey('maries.uuid'),
        nullable=True)
    famille_uuid = Column(
        UUID(
            as_uuid=True),
        ForeignKey('famille.uuid'),
        nullable=True)

    role = 'invite'

    __mapper_args__ = {
        'polymorphic_identity': 'invites',
    }


class Administrateur(User):
    __tablename__ = "administrateurs"

    uuid = Column(
        UUID(
            as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
        index=True)
    role = 'admin'

    __mapper_args__ = {
        'polymorphic_identity': 'administrateurs',
    }

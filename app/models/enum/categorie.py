import enum


class Categorie(enum.Enum):
    famille = "Famille"
    amis = "Amis"
    autre = "Autre"

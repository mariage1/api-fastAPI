import enum


class Importance(enum.Enum):
    tres_important = "TI"
    important = "I"
    facultatif = "F"

import graphene
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from graphql.execution.executors.asyncio import AsyncioExecutor
from starlette.graphql import GraphQLApp

from app import models, logger
from app.database import engine
from app.graphQL.mutation import Mutation
from app.graphQL.query import Query
from app.middleware.limite_upload_size import LimitUploadSize
from app.routes.invites_routes import router
from app.routes.security_routes import security_router
from app.services.sutep_create_marier_service import create_default_marier
from app.websocket.active_user_websocket import websocket_endpoint

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.on_event("startup")
async def startup_event():
    logger.debug("startup action")
    create_default_marier()


origins = [
    "http://localhost",
    "http://localhost:8080",
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.add_middleware(LimitUploadSize, max_upload_size=50_000_000)

app.add_route("/graphql/", GraphQLApp(
    executor_class=AsyncioExecutor,
    schema=graphene.Schema(query=Query,
                           mutation=Mutation,)))

app.include_router(router)
app.include_router(security_router)

app.add_api_websocket_route('/webSocket/activate', websocket_endpoint)

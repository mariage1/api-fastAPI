from fastapi import UploadFile, File, APIRouter, HTTPException, Depends
from starlette.responses import FileResponse

from app.crud.invite_crud import create_invites
from app.database import SessionLocal
from app.middleware.Authorization import rest_security_all_privilege
from app.services.file_factory_service import csv_file_to_dict
from app import logger
from app.services.invitation_service import make_invitation

router = APIRouter(
    prefix="/rest/invites"
)


@router.post("/uploadfile")
async def create_upload_file(file: UploadFile = File(...), token: bool = Depends(rest_security_all_privilege)):
    liste = await csv_file_to_dict(file)
    stat, message = create_invites(db=SessionLocal(), invites=liste)

    if not stat:
        logger.error(message)
        raise HTTPException(
            status_code=500,
            detail={'stat': stat, 'message': message})

    return {'stat': stat, 'message': message}


@router.get("/invitation")
async def create_invitation(generate_invitation: bool = True, token: bool = Depends(rest_security_all_privilege)):
    if generate_invitation:
        await make_invitation()
    file = FileResponse("./data/pdf/invitations.pdf", media_type='application/pdf',
                        filename='invitation.pdf')
    return file

from datetime import timedelta

from fastapi import Depends, HTTPException, status, APIRouter
from fastapi.security import OAuth2PasswordRequestForm

from app import setting

from app.models.token_model import Token
from app.services.oauth2_service import authenticate_user, create_access_token

security_router = APIRouter(
    prefix="/rest/security"
)


@security_router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(hours=setting.token_jwt_expire_hours)
    access_token = create_access_token(
        data={
            "sub": str(user.uuid),
            "role": user.role,
        }, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

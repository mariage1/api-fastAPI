from sqlalchemy.orm import Session

from app import logger, setting
from app.services.oauth2_service import get_password_hash
from app.crud.maries_crud import get_maries, create_maries
from app.database import SessionLocal


def create_default_marier():
    db: Session = SessionLocal()
    maries = get_maries(db)

    if len(maries) == 0:
        antoine = {
            "last_name": "Berthier",
            "first_name": "Antoine",
            "hashed_password": get_password_hash(
                setting.default_marier_password),
            "email": "email"}

        manon = {
            "last_name": "Chapuis",
            "first_name": "Manon",
            "hashed_password": get_password_hash(
                setting.default_marier_password),
            "email": "email"}
        create_maries(db=db, liste_maries=[antoine, manon])

        logger.info("marier crée")
    db.close()

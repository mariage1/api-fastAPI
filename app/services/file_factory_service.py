import codecs
import csv

from fastapi import UploadFile

from app.services.oauth2_service import get_password_hash


async def csv_file_to_dict(scv_file: UploadFile):
    data_tab = []
    data = scv_file.file
    data = csv.reader(codecs.iterdecode(data, 'utf-8'), delimiter=',')
    for row in data:
        data_tab.append({
            'info': {
                'first_name': row[1],
                'last_name': row[2],
                'marie': row[3],
                'categorie': row[4],
                'importance': row[5],
                'email': row[6],
                'hashed_password': get_password_hash(row[1] + '#' + row[2])
            },
            'famille': row[2],
        })
    data_tab.pop(0)
    return data_tab

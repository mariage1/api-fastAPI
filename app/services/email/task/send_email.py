from datetime import datetime

from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

from app import logger, setting
from app.models.users_models import Invite
from app.services.email.email_service import EmailService
from app.services.oauth2_service import get_password_hash, get_random_password

templates = Jinja2Templates(directory="templates/html")


async def task_activate_invite(db: Session, invite: Invite) -> None:
    logger.debug("Task activate invite")
    password: str = get_random_password()
    invite.is_active = True
    invite.hashed_password = get_password_hash(password)
    message_template = templates.get_template("email_activate_invite.html")
    message = message_template.render({
        "invite": invite,
        "password": password,
        "email": setting.gmail_user,
        "url": setting.client_url
    })
    email_service = EmailService()
    await email_service.send_message(
        [invite.email],
        message,
        "Mariage password temporaire"
    )
    db.add(invite)
    db.commit()
    db.close()
    logger.debug(datetime.now())


async def task_error_activate_invite(invite: Invite) -> None:
    logger.debug("Task error activate invite")
    message_template = templates.get_template(
        "email_error_activate_invite.html")
    message = message_template.render({
        "invite": invite,
    })
    email_service = EmailService()
    await email_service.send_message(
        [setting.gmail_user],
        message,
        "[ALERTE] Problemme dans l'activation d'un compte utilisateur"
    )

import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import List

from app import setting


class EmailService:
    _port: int = 587
    _password: str = setting.gmail_password
    _sender_email: str = setting.gmail_user

    async def send_message(self, receiver_email: List[str], message_html: str, subject: str) -> None:
        context = ssl.create_default_context()

        message = MIMEMultipart()
        message['Subject'] = subject
        message['From'] = self._sender_email
        message['To'] = ','.join(receiver_email)

        message.attach(MIMEText(message_html, "html"))
        msg_body = message.as_string()

        with smtplib.SMTP("smtp.gmail.com", self._port) as server:
            server.starttls(context=context)
            server.login(self._sender_email, self._password)
            server.sendmail(self._sender_email, receiver_email, msg_body)
        server.close()

import os

import qrcode

from app import setting


async def generate_qr_code(uuid_invite_liste):
    client_url = setting.client_url
    if not os.path.isdir('./data'):
        os.makedirs('./data/images')
        os.makedirs('./data/pdf')

    for uuid in uuid_invite_liste:
        qr_string = "{}activate/{}".format(client_url, uuid[0])
        img = qrcode.make(qr_string)
        path = "./data/images/qr_{}.jpg".format(uuid[0])
        img.save(path, "JPEG")

from pylatex import Document, NoEscape, Package
from sqlalchemy.orm import Session
from starlette.templating import Jinja2Templates

from app.crud.invite_crud import get_uuid_list
from app.database import SessionLocal
from app.services.qr_code.generate_qr_code import generate_qr_code

templates = Jinja2Templates(
    directory="templates/latex"
)


async def make_invitation():
    db: Session = SessionLocal()
    uuids = get_uuid_list(db=db)
    await generate_qr_code(uuids)

    templates.env.block_start_string = '\BLOCK{'
    templates.env.block_end_string = '}'

    templates.env.variable_start_string = '\VAR{'
    templates.env.variable_end_string = '}'

    templates.env.comment_start_string = '\#{'
    templates.env.comment_end_string = '}'

    templates.env.line_statement_prefix = '%%'
    templates.env.line_comment_prefix = '%#'

    templates.env.trim_blocks = True
    templates.env.autoescape = False

    invitations_template = templates.get_template('invitation_invite.tex')
    invitations_latex = invitations_template.render({"uuids": [str(uuid[0]) for uuid in uuids]})
    doc = Document()

    doc.packages.append(Package('graphicx'))
    doc.append(NoEscape(invitations_latex))

    doc.generate_tex('./data/save')

    doc.generate_pdf(filepath='./data/pdf/invitations', clean_tex=False)

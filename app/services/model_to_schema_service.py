from app.models.users_models import Invite, Marie, Famille


def traduction_invite(invite: Invite):
    traduction = {
        'uuid': invite.uuid,
        'first_name': invite.first_name,
        'last_name': invite.last_name,
        'email': invite.email,
        'datetime_create_user': invite.datetime_create_user,
        'datetime_last_connection': invite.datetime_last_connection,
        'categorie': invite.categorie.value,
        'importance': invite.importance.value,
        'has_validate': invite.has_validate,
        'famille': invite.famille,
        'marie': invite.maries_groupe
    }
    return traduction


def traduction_marier(marie: Marie):
    return {
        'uuid': marie.uuid,
        'first_name': marie.first_name,
        'last_name': marie.last_name,
        'email': marie.email,
        'datetime_create_user': marie.datetime_create_user,
        'datetime_last_connection': marie.datetime_last_connection,
        'invites': marie.invites
    }


def traduction_famille(famille: Famille):
    return {
        'uuid': famille.uuid,
        'name': famille.name,
        'membres': famille.membres,
        'famille_email': famille.famille_email
    }


def traduction_user(user):
    if isinstance(user, Invite):
        return traduction_invite(user)
    if isinstance(user, Marie):
        return traduction_marier(user)
    return None

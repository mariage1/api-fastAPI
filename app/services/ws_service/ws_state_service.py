from __future__ import annotations

import asyncio
import uuid
from abc import ABC, abstractmethod

from sqlalchemy.orm import Session
from starlette.websockets import WebSocket

from app import logger
from app.crud.invite_crud import get_invite
from app.database import SessionLocal
from app.models.users_models import Invite
from app.services.email.task.send_email import task_activate_invite, task_error_activate_invite
from app.services.util_service import secure_email


class Context:
    """
    The Context defines the interface of interest to clients. It also maintains
    a reference to an instance of a State subclass, which represents the current
    state of the Context.
    """

    _state = None
    """
    A reference to the current state of the Context.
    """

    def __init__(self, state: State) -> None:
        self.transition_to(state)

    def transition_to(self, state: State):
        """
        The Context allows changing the State object at runtime.
        """

        logger.debug(f"Context: Transition to {type(state).__name__}")
        self._state = state
        self._state.context = self

    """
    The Context delegates part of its behavior to the current State object.
    """

    async def action(self):
        return await self._state.action()


class State(ABC):
    _ws: WebSocket
    _last: bool

    @property
    def context(self) -> Context:
        return self._context

    @context.setter
    def context(self, context: Context) -> None:
        self._context = context

    @property
    def last(self) -> bool:
        return self._last

    @last.setter
    def last(self, last: bool) -> None:
        self._last = last

    @property
    def ws(self) -> WebSocket:
        return self._ws

    @ws.setter
    def ws(self, ws: WebSocket):
        self._ws = ws

    @abstractmethod
    async def action(self) -> bool:
        pass


class StateStart(State):

    def __init__(self, ws: WebSocket):
        self.ws = ws
        self.last = False

    async def action(self) -> bool:
        try:
            await self.ws.accept()
            receive = await self.ws.receive_json()
            if "uuid" in receive:
                self.context.transition_to(
                    StateSendEmail(
                        self.ws, uuid.UUID(
                            receive["uuid"])))
            else:
                self.context.transition_to(StateError(self.ws, "UUID inconu"))
            return self.last
        except AssertionError:
            logger.error("Error Websocket Assertion")
            await self.ws.close()
            raise Exception("Error Websocket Assertion")


class StateSendPassword(State):
    def __init__(self, ws: WebSocket, db: Session, invite: Invite):
        self.ws = ws
        self.last = False
        self._invite = invite
        self._db = db

    async def action(self) -> bool:
        asyncio.create_task(task_activate_invite(self._db, self._invite))
        await self.ws.send_json(data={
            "stat": "end",
            "message": "Activation de l'utilisateur",
        })

        self.context.transition_to(StateEnd(self.ws))
        return self.last


class StateSendEmail(State):

    def __init__(self, ws: WebSocket, invite_uuid):
        self.ws = ws
        self.last = False
        self._uuid = invite_uuid

    async def action(self) -> bool:
        logger.debug(str(self._uuid))
        db: Session = SessionLocal()
        invite: Invite = get_invite(
            db=db,
            invite_uuid=self._uuid)

        if invite.is_active:
            self.context.transition_to(
                StateError(
                    self.ws,
                    "l'Utilisateur est déjà activer"))
            db.close()
            return self.last

        email_cache = secure_email(invite.email)
        await self.ws.send_json(data={
            "stat": "Email",
            "message": email_cache
        })
        data = await self.ws.receive_json()
        if data['value']:
            self.context.transition_to(StateSendPassword(self.ws, db, invite))
        else:
            asyncio.create_task(task_error_activate_invite(invite))
            self.context.transition_to(
                StateError(
                    self.ws,
                    "Email refusé par l'utilisateur"))
            db.close()
        return self.last


class StateError(State):
    def __init__(self, ws: WebSocket, message: str):
        self.ws = ws
        self.last = True
        self.message = message

    async def action(self) -> bool:
        logger.info(self.message)
        await self.ws.send_json(data={
            "stat": "Error",
            "message": self.message,
        })
        await self.ws.close()
        return self.last


class StateEnd(State):
    def __init__(self, ws: WebSocket):
        self.ws = ws
        self.last = True

    async def action(self) -> bool:
        logger.debug('Close socket')
        await self.ws.close()
        return self.last

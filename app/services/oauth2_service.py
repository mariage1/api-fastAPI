import random
import string
from datetime import datetime, timedelta
from typing import Optional

from jose import jwt, JWTError
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from app import setting
from app.crud.user_crud import get_user, get_user_by_email
from app.database import SessionLocal

pwd_context = CryptContext(
    schemes=["bcrypt"],
    bcrypt__default_rounds=7,
    deprecated="auto")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_random_password() -> str:
    letters = string.ascii_lowercase + string.digits
    return ''.join(random.choice(letters) for i in range(10))


def authenticate_user(invite_email: str, password: str):
    user = get_user_by_email(db=SessionLocal(), email=invite_email)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(hours=6)
    to_encode.update({
        "exp": expire
    })
    encoded_jwt = jwt.encode(
        to_encode,
        setting.secret_key,
        algorithm=setting.token_jwt_algo)
    return encoded_jwt


def get_current_user(db: Session, token):
    try:
        payload = jwt.decode(
            token, setting.secret_key, algorithms=[
                setting.token_jwt_algo])
        uuid = payload.get("sub")
        if uuid is None:
            raise Exception("JWT Token is not valide")
    except JWTError:
        raise Exception("JWT Token is not valide")
    user = get_user(db, user_uuid=uuid)
    if user is None:
        return "uuid is not found"
    return user


def get_current_active_user(db: Session, token):
    stat, current_user = get_current_user(db, token)
    if not stat:
        return stat, current_user
    if not current_user.is_active:
        return False, "Inactive user"
    return current_user

import re


def convert(tup, di):
    for a, b in tup:
        di.setdefault(a, []).append(b)
    return di


def secure_email(email: str) -> str:
    tab_email = email.split("@")
    len_name_email = str(round(len(tab_email[0]) / 6))
    len_domaine_email = str(round(len(tab_email[1]) / 6))
    regex = r"(.{" + len_name_email + "})(.*)(.{" + \
        len_name_email + "})(@.)(.{" + len_domaine_email + "})(.*)"
    new_regex = r"\1***\3\4***\6"
    return re.sub(regex, new_regex, email)

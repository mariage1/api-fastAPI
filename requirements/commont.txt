fastapi~=0.63.0
uvicorn[standard]~=0.13.4
python-multipart
sqlalchemy~=1.3.23

graphene~=2.1.8

pydantic~=1.7.3
starlette~=0.13.6
psycopg2

python-jose[cryptography]
passlib[bcrypt]~=1.7.4

requests

jose~=1.0.0
Rx~=1.6.1

graphql-ws
jinja2
aiofiles


